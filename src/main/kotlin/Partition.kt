import com.pajato.ctci.Node

fun partitionList(partition: Int, head: Node) {
    var previous = head
    var swapIsEnabled = head.value >= partition
    tailrec fun processNodes(current: Node?) {
        fun swap() {
            fun swapValues() {
                val tmp = head.value
                head.value = current!!.value
                current.value = tmp
            }
            fun swapNodes() {
                val tmpValue = head.value
                val tmpNext = head.next
                fun bypassCurrent() { previous.next = current!!.next }
                fun setHead() {
                    head.value = current!!.value
                    head.next = current
                }
                fun setCurrent() {
                    current!!.value = tmpValue
                    current.next = tmpNext
                }

                bypassCurrent()
                setHead()
                setCurrent()
            }

            // Save what will become the second node.
            if (!swapIsEnabled) return
            if (previous == head) swapValues() else swapNodes()
        }

        if (current == null) return
        val tmp = current.next
        if (!swapIsEnabled && current.value >= partition) swapIsEnabled = true
        if (current.value < partition) swap() else previous = current
        processNodes(tmp)
    }

    processNodes(head.next)
}
