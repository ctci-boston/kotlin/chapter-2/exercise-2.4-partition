import com.pajato.ctci.Node
import com.pajato.ctci.createLinkedList
import com.pajato.ctci.isEqual
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

class Test {

    @Test fun `verify an empty linked list can be created`() {
        assertEquals(null, createLinkedList())
    }

    @Test fun `verify that a linked list with one node can be created`() {
        val expected = Node(0)
        val actual = createLinkedList(0)
        assertEquals(expected.value, actual?.value ?: fail("The actual node is null!"))
        assertEquals(expected.next, actual.next)
    }

    @Test fun `verify a list with two nodes can be created`() {
        val expected = Node(1)
        val expectedNext = Node(2)
        expected.next = expectedNext

        val actual = createLinkedList(1, 2)
        val actualNext = actual?.next
        assertEquals(expected.value, actual?.value ?: fail("The actual node is null!"))
        assertEquals(expectedNext.value, actual.next?.value ?: fail("Second node is wrong!"))
        assertEquals(null, actualNext?.next)
    }

    @Test fun `verify a long list can be created correctly`() {
        val actual = createLinkedList(1, 2, 3, 4, 5, 6, 7, 8)
        assertEquals(1, actual?.value ?: fail("The first node is null!"))
        assertEquals(2, actual.next?.value ?: fail("The second node is null!"))
        assertEquals(3, actual.next?.next?.value ?: fail("The third node is null!"))
        assertEquals(4, actual.next?.next?.next?.value ?: fail("The fourth node is null!"))
        assertEquals(5, actual.next?.next?.next?.next?.value ?: fail("The fifth node is null!"))
        assertEquals(6, actual.next?.next?.next?.next?.next?.value ?: fail("The sixth node is null!"))
        assertEquals(7, actual.next?.next?.next?.next?.next?.next?.value ?: fail("The seventh node is null!"))
        assertEquals(8, actual.next?.next?.next?.next?.next?.next?.next?.value ?: fail("The eighth node is null!"))
        val tail = actual.next?.next?.next?.next?.next?.next?.next ?: fail("the list does not terminate correctly!")
        assertEquals(tail.next, null)
    }

    @Test fun `verify two identical long lists are equal`() {
        val list1 = createLinkedList(1, 2, 3, 4, 5, 6, 7)
        val list2 = createLinkedList(1, 2, 3, 4, 5, 6, 7)
        assertTrue(isEqual(list1, list2))
    }

    @Test fun `verify two different long lists are not equal`() {
        val list1 = createLinkedList(1, 2, 3, 4, 5, 6, 8)
        val list2 = createLinkedList(1, 2, 3, 4, 5, 6, 7)
        assertFalse(isEqual(list1, list2))
    }

    @Test fun `When the given list starts with a left partition value verify the result`() {
        val head = createLinkedList(3, 5, 8, 5, 10, 2, 1)
        val expected = createLinkedList(1, 2, 3, 5, 8, 5, 10)

        partitionList(5, head!!)
        assertTrue(isEqual(expected, head))
    }

    @Test fun `When the given list starts with consecutive left partition values verify the result`() {
        val head = createLinkedList(3, 2, 1, 8, 5, 10, 2, 1)
        val expected = createLinkedList(1, 2, 3, 2, 1, 8, 5, 10)

        partitionList(5, head!!)
        assertTrue(isEqual(expected, head))
    }

    @Test fun `When the given list starts with a right partition value verify the result`() {
        val head = createLinkedList(15, 3, 5, 8, 5, 10, 2, 1)
        val expected = createLinkedList(1, 2, 3, 15, 5, 8, 5, 10)

        partitionList(5, head!!)
        assertTrue(isEqual(expected, head))
    }
}
